package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("stockDatabase")
public interface StockDatabaseService extends RemoteService
{
	boolean storeStock(String stock);
}
