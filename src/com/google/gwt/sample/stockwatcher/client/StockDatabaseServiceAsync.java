package com.google.gwt.sample.stockwatcher.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface StockDatabaseServiceAsync 
{
	void storeStock(String stock, AsyncCallback<Boolean> callback);
}
