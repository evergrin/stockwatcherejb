package com.google.gwt.sample.stockwatcher.server;

import javax.ejb.EJB;

import com.google.gwt.sample.stockwatcher.client.StockDatabaseService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class StockDatabaseServiceImpl extends RemoteServiceServlet implements StockDatabaseService
{
	private IStockPrice iStockPrice;
	
	@EJB(name ="IStockPrice")
	public void setIStockPrice(IStockPrice iStockPrice)
	{
		this.iStockPrice = iStockPrice;
	}
	
	public StockDatabaseServiceImpl() {}
	
	public boolean storeStock(String stock) 
	{
    	System.out.println("ENTERED function storeStock(String '"+ stock +"')" );
		
		StockPriceEntity stockPrice = new StockPriceEntity( stock );

		if( iStockPrice == null)
		   	System.err.println("MISSING interface iStockPrice!");
	    		
		iStockPrice.createStock(stockPrice);
		
		return true;
	}
}
