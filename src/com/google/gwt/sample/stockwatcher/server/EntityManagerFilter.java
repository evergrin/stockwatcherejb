package com.google.gwt.sample.stockwatcher.server;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EntityManagerFilter implements Filter  
{  
//    private Logger itsLogger = Logger.getLogger(getClass().getName());  
    private static EntityManagerFactory theEntityManagerFactory = null;  
  
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)  
        throws IOException, ServletException  
    {  
        EntityManager em = null;  
        try  
        {  
            em = theEntityManagerFactory.createEntityManager();  
            EntityManagerUtil.ENTITY_MANAGERS.set(em);  
            chain.doFilter(request,response);  
            EntityManagerUtil.ENTITY_MANAGERS.remove();  
        }  
        finally  
        {  
            try  
            {  
                if (em != null)  
                    em.close();  
            }  
            catch (Throwable t) 
            {  
//                itsLogger.error("While closing an EntityManager",t);  
            }  
        }  
    }  
    public void init(FilterConfig config)  
    {  
        destroy();  
        theEntityManagerFactory = Persistence.createEntityManagerFactory("gliffy");  
    }  
    public void destroy()  
    {  
        if (theEntityManagerFactory != null)  
            theEntityManagerFactory.close();  
    }  
}  