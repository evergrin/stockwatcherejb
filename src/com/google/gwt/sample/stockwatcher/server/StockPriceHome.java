package com.google.gwt.sample.stockwatcher.server;

import static javax.ejb.TransactionAttributeType.REQUIRED;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;

@Stateless(name="IStockPrice")
@Remote(IStockPrice.class)
public class StockPriceHome implements Serializable, IStockPrice 
{
    protected EntityManager entityManager = EntityManagerUtil.getEntityManager();

    @SuppressWarnings("unchecked")
	@TransactionAttribute(REQUIRED)
	public List<StockPriceEntity> getStockPrices()
	{
    	System.out.println("ENTERED function getStockPrices()" );
	    return entityManager.createQuery("SELECT s FROM StockPrice s").getResultList();
	}
    
    @SuppressWarnings("unchecked")
	@TransactionAttribute(REQUIRED)
	public StockPriceEntity getStockPriceBy(String symbol)
	{
    	System.out.println("ENTERED function getStockPriceBy for SYMBOL" + symbol );
	    return (StockPriceEntity) entityManager.createQuery("SELECT s FROM StockPrice s WHERE s.symbol = :symbol ORDER BY v.id").setParameter("symbol", symbol).getResultList().get(0);
	}
    
    public void createStock(StockPriceEntity stock)
    {
    	System.out.println("ENTERED function createStock for stock with SYMBOL" + stock.getSymbol() );
    	entityManager.persist(stock);
    }

    public void updateStock(StockPriceEntity stock)
    {
    	System.out.println("ENTERED function updateStock for stock with SYMBOL" + stock.getSymbol() );
    	entityManager.merge(stock);
    }
}
