package com.google.gwt.sample.stockwatcher.server;

import java.util.List;

public interface IStockPrice 
{
	List<StockPriceEntity> getStockPrices();
	StockPriceEntity getStockPriceBy(String symbol);
	void createStock(StockPriceEntity stock);
	void updateStock(StockPriceEntity stock);
}
